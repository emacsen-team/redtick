The Debian packaging of redtick is maintained in git, using the
merging workflow described in dgit-maint-merge(7).  There isn't a
patch queue that can be represented as a quilt series.

A detailed breakdown of the changes is available from their canonical
representation - git commits in the packaging repository.  For
example, to see the changes made by the Debian maintainer in the first
upload of upstream version 1.2.3, you could use:

    % git clone https://git.dgit.debian.org/redtick
    % cd redtick
    % git log --oneline 1.2.3..debian/1.2.3-1 -- . ':!debian'

(If you have dgit, use `dgit clone redtick`, rather than plain `git
clone`.)

A single combined diff, containing all the changes, follows.
--- redtick-00.01.02+git20170220.e6d2e9b+dfsg.orig/README.md
+++ redtick-00.01.02+git20170220.e6d2e9b+dfsg/README.md
@@ -5,20 +5,16 @@ RedTick
 
 This package provides a little pomodoro timer in the mode-line.
 
-![Redtick](https://raw.githubusercontent.com/ferfebles/redtick/master/redtick.gif)
+![Redtick](redtick.gif)
 
 ## How
 
-* Install from melpa (http://melpa.org/#/getting-started)
-
 * Use `(require 'redtick)`. After requiring, it shows a little red tick (✓) in the mode-line. When
 you click on it, it starts a pomodoro timer. The pomodoro description is set using current-buffer and which-function.
-![Redtick description](https://raw.githubusercontent.com/ferfebles/redtick/master/redtick-description.png)
+![Redtick description](redtick-description.png)
 
 * You can launch a pomodoro by M-x redtick, or M-x redtick-with-description (manual description).
 
-You should install SoX (Sound eXchange http://sox.sourceforge.net) if you want to hear the clock ticking! (be careful, in windows you should clear the "" in the sox path to allow emacs to find the executable)
-
 It only shows the timer in the selected window (a moving timer
 replicated in each window is a little bit distracting!).
 
--- redtick-00.01.02+git20170220.e6d2e9b+dfsg.orig/redtick.el
+++ redtick-00.01.02+git20170220.e6d2e9b+dfsg/redtick.el
@@ -83,17 +83,17 @@
   :type 'string)
 (defcustom redtick-work-sound
   (expand-file-name "./resources/work.wav"
-                    (file-name-directory (or load-file-name buffer-file-name)))
+                    "/usr/share/redtick")
   "Sound file to loop during the work period."
   :type 'string)
 (defcustom redtick-rest-sound
   (expand-file-name "./resources/rest.wav"
-                    (file-name-directory (or load-file-name buffer-file-name)))
+                    "/usr/share/redtick")
   "Sound file to loop during the rest period."
   :type 'string)
 (defcustom redtick-end-rest-sound
-  (expand-file-name "./resources/end-rest.mp3"
-                    (file-name-directory (or load-file-name buffer-file-name)))
+  (expand-file-name "./resources/rest.wav"
+                    "/usr/share/redtick")
   "Sound file to play at the end of the rest period."
   :type 'string)
 
@@ -160,7 +160,7 @@
 (defun redtick--play-sound-during (file seconds)
   "Play FILE during SECONDS, repeating or cutting if needed."
   (let ((fade (if (< seconds 8) "0" "4")))
-       (redtick--play-sound file `("repeat" "-" "fade" "t" ,fade
+       (redtick--play-sound file `("repeat" "fade" "t" ,fade
                                    ,(number-to-string seconds)))))
 
 (defun redtick--play-work-sound ()
@@ -249,14 +249,15 @@
 
 (defun redtick--save-history ()
   "Adding current-pomodoro info to history file."
-  (let ((history (redtick--load redtick-history-file)))
-    (redtick--save redtick-history-file
-                   (add-to-list 'history
-                                (list redtick--pomodoro-started-at
-                                      redtick-work-interval
-                                      redtick-rest-interval
-                                      redtick--pomodoro-description)
-                                t))))
+  (when redtick-history-file
+    (let ((history (redtick--load redtick-history-file)))
+      (redtick--save redtick-history-file
+                     (add-to-list 'history
+                                  (list redtick--pomodoro-started-at
+                                        redtick-work-interval
+                                        redtick-rest-interval
+                                        redtick--pomodoro-description)
+                                  t)))))
 
 (add-hook 'redtick-after-rest-hook #'redtick--save-history)
 
